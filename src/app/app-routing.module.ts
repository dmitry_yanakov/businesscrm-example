import { NoAuthGuard } from './core/guards/no-auth-guard';
import { AuthGuard } from './core/guards/auth-guard';
import { DeskComponent } from './pages/desk/desk.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/auth/components/login/login.component';
import { RegistrationComponent } from './pages/auth/components/registration/registration.component';
import { MoreComponent } from './pages/auth/components/more/more.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { UserSettingsComponent } from './pages/user-profile/components/user-settings/user-settings.component';
import { UserSecurityComponent } from './pages/user-profile/components/user-security/user-security.component';
import { ForgotPasswordComponent } from './pages/auth/components/forgot-password/forgot-password.component';
import { CreateCompanyComponent } from './pages/create-company/create-company.component';
import { CreateLocationComponent } from './pages/create-location/create-location.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ChangeForgotComponent } from './pages/auth/components/change-forgot/change-forgot.component';

import { EmployeesComponent } from './pages/employees/employees.component';
import { TableActiveEmployeesComponent } from './pages/employees/components/table-active-employees/table-active-employees.component';
import { TableFiredEmployeesComponent } from './pages/employees/components/table-fired-employees/table-fired-employees.component';
import { CreateEmployeeComponent } from './pages/employees/create-employee/create-employee.component';
import { EmployeeCardComponent } from './pages/employees/employee-card/employee-card.component';
import { TableGoodsComponent } from './pages/products/products-lists/table-goods/table-goods.component';
import { TableServicesComponent } from './pages/products/products-lists/table-services/table-services.component';
import { TableServicePackagesComponent } from './pages/products/products-lists/table-service-packages/table-service-packages.component';
import { TablePassesComponent } from './pages/products/products-lists/table-passes/table-passes.component';
import { TableClassesComponent } from './pages/products/products-lists/table-classes/table-classes.component';
import { ProductsComponent } from './pages/products/products.component';
import { TableArchiveComponent } from './pages/products/products-lists/table-archive/table-archive.component';
import { ProductsListsComponent } from './pages/products/products-lists/products-lists.component';
import { CreateProductComponent } from './pages/products/create/create-product/create-product.component';
import { EditProductComponent } from './pages/products/edit/edit-product/edit-product.component';
import { GeneralComponent } from './pages/payments/general/general.component';
import { PaymentHistoryComponent } from './pages/payments/payment-history/payment-history.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { ImportsComponent } from './pages/imports/imports.component';
import { SmsComponent } from './pages/sms/sms.component';
import { IncomeComponent } from './pages/finance/income/income.component';
import { CostsComponent } from './pages/finance/costs/costs.component';
import { FinanceComponent } from './pages/finance/finance.component';
import { NewSaleComponent } from './pages/finance/new-sale/new-sale.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { ClientsListComponent } from './pages/clients/components/clients-list/clients-list.component';
import { CreateClientComponent } from './pages/clients/components/create-client/create-client.component';
import { ClientCardComponent } from './pages/clients/components/client-card/client-card.component';
import { ClientStatisticsComponent } from './pages/clients/components/client-card/client-tabs/client-statistics/client-statistics.component';
import { ClientNotesComponent } from './pages/clients/components/client-card/client-tabs/client-notes/client-notes.component';
import { ClientSessionsComponent } from './pages/clients/components/client-card/client-tabs/client-sessions/client-sessions.component';
import { ClientPurchasesComponent } from './pages/clients/components/client-card/client-tabs/client-purchases/client-purchases.component';
import { ClientTicketsComponent } from './pages/clients/components/client-card/client-tabs/client-tickets/client-tickets.component';
import { AddProductsComponent } from './pages/finance/add-products/add-products.component';
import { NewServicesComponent } from './pages/finance/new-services/new-services.component';
import { NewGoodsComponent } from './pages/finance/new-goods/new-goods.component';
import { ScheduleComponent } from './pages/schedule/schedule.component';
import { CreateSessionComponent } from './pages/schedule/session/create-session/create-session.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard] },
  { path: 'registration', component: RegistrationComponent, canActivate: [NoAuthGuard] },
  { path: 'more', component: MoreComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [NoAuthGuard] },
  { path: 'changeforgot', component: ChangeForgotComponent, canActivate: [NoAuthGuard] },
  { path: 'desk', component: DeskComponent, canActivate: [AuthGuard] },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'settings', component: UserSettingsComponent, canActivate: [AuthGuard] },
      { path: 'security', component: UserSecurityComponent, canActivate: [AuthGuard] },
    ],
  },
  { path: 'create-company', component: CreateCompanyComponent, canActivate: [AuthGuard] },
  { path: 'create-location/:id', component: CreateLocationComponent, canActivate: [AuthGuard] },
  {
    path: 'staff',
    component: EmployeesComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'active', component: TableActiveEmployeesComponent, canActivate: [AuthGuard] },
      { path: 'fired', component: TableFiredEmployeesComponent, canActivate: [AuthGuard] },
    ],
  },
  { path: 'staff/:id', component: EmployeeCardComponent, canActivate: [AuthGuard] },
  { path: 'create-staff-member', component: CreateEmployeeComponent, canActivate: [AuthGuard] },
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: ProductsListsComponent,
        canActivate: [AuthGuard],
        children: [
          { path: 'goods', component: TableGoodsComponent, canActivate: [AuthGuard] },
          { path: 'services', component: TableServicesComponent, canActivate: [AuthGuard] },
          { path: 'service-packages', component: TableServicePackagesComponent, canActivate: [AuthGuard] },
          { path: 'passes', component: TablePassesComponent, canActivate: [AuthGuard] },
          { path: 'classes', component: TableClassesComponent, canActivate: [AuthGuard] },
          { path: 'archive', component: TableArchiveComponent, canActivate: [AuthGuard] },
        ],
      },
      { path: 'create/:product-type', component: CreateProductComponent, canActivate: [AuthGuard] },
      {
        path: 'edit/:product-type/:product-id/:selected-tab',
        component: EditProductComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: 'clients',
    component: ClientsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: ClientsListComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: 'client-card/:id',
    component: ClientCardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'statistics', component: ClientStatisticsComponent, canActivate: [AuthGuard] },
      { path: 'notes', component: ClientNotesComponent, canActivate: [AuthGuard] },
      { path: 'sessions', component: ClientSessionsComponent, canActivate: [AuthGuard] },
      { path: 'purchases', component: ClientPurchasesComponent, canActivate: [AuthGuard] },
      { path: 'tickets', component: ClientTicketsComponent, canActivate: [AuthGuard] },
    ],
  },
  {
    path: 'schedule',
    component: ScheduleComponent,
    canActivate: [AuthGuard],
    children: [{ path: 'create/:session-type', component: CreateSessionComponent, canActivate: [AuthGuard] }],
  },
  { path: 'client-create', component: CreateClientComponent, canActivate: [AuthGuard] },
  { path: 'client-update/:id', component: CreateClientComponent, canActivate: [AuthGuard] },
  {
    path: 'payments',
    component: PaymentsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'general', component: GeneralComponent, canActivate: [AuthGuard] },
      { path: 'history', component: PaymentHistoryComponent, canActivate: [AuthGuard] },
    ],
  },
  {
    path: 'finance',
    component: FinanceComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'income', component: IncomeComponent, canActivate: [AuthGuard] },
      { path: 'costs', component: CostsComponent, canActivate: [AuthGuard] },
    ],
  },
  {
    path: 'new-sale',
    component: NewSaleComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'goods', component: NewGoodsComponent, canActivate: [AuthGuard] },
      { path: 'services', component: NewServicesComponent, canActivate: [AuthGuard] },
    ],
  },
  { path: 'add-products', component: AddProductsComponent, canActivate: [AuthGuard] },
  { path: 'database-import', component: ImportsComponent, canActivate: [AuthGuard] },
  { path: 'sms', component: SmsComponent, canActivate: [AuthGuard] },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
