import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs';

// Interface
import { SelectItem } from 'primeng/api';

// Service
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { CompanyService } from '@core/services/company.service';
import { CommonDataService } from '@core/services/common-data.service';
import { take } from 'rxjs/operators';
import { NotificationMessageService } from '@core/services/notification-message.service';

interface Industry {
  id: number;
  category: string;
  title: string;
  type: number;
}

interface IndustryCategoriesList {
  label: string;
  items: Array<{
    label: string;
    value: number;
  }>;
}

interface Currency {
  code: string;
  course: number;
  id: number;
  symbol: string;
  title: string;
}

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
})
export class GeneralComponent implements OnInit {
  companyProfile: FormGroup = null;

  currencies = [];
  industries = [];
  companyTypes = {
    1: 'company-type-entrepreneur',
    2: 'company-type-small-business',
    3: 'company-type-network-business',
  };
  languages = [
    { label: 'Русский', value: 'ru', img: 'assets/flags/248-russia.svg' },
    { label: 'English', value: 'en', img: 'assets/flags/226-united-states.svg' },
    { label: 'French', value: 'fr', img: 'assets/flags/195-france.svg' },
  ];
  companyType;

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private translate: TranslateService,
    private commonData: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.initForm();

    this.commonData.companyUpdate.subscribe(() => {
      this.updateComponent();
    });
  }

  initForm() {
    this.companyProfile = this.fb.group({
      logo: '',
      title: this.commonData.currentCompany.title,
      industry_id: this.commonData.currentCompany.industry_id,
      currency_id: this.commonData.currentCompany.currency_id,
      language: this.commonData.user.language,
      description: this.commonData.currentCompany.description,
      email: this.commonData.currentCompany.email,
      site_url: this.commonData.currentCompany.site_url,
      fb_url: this.commonData.currentCompany.fb_url,
      ig_url: this.commonData.currentCompany.ig_url,
    });

    this.companyType = this.companyTypes[this.commonData.currentCompany['type_id']];

    this.getCompanyLogo();
    this.getIndustry();
    this.getCurrencies();
  }

  updateComponent() {
    this.companyType = this.companyTypes[this.commonData.currentCompany['type_id']];

    this.companyProfile.patchValue(this.commonData.currentCompany);
    this.getCompanyLogo();
    this.getIndustry();
  }

  getCompanyLogo() {
    this.api
      .getCompanyLogo(this.commonData.currentCompany.id)
      .pipe(take(1))
      .subscribe((response) => {
        this.companyProfile.patchValue({ logo: response });
      });
  }

  // Индустрии делятся на две категории:
  // 0 - Для малого бизнеса(type_id=2) и предприятия(type_id=3)
  // 1 - Для индивидуального предпринимателя(type_id=1)
  getIndustry() {
    const business_type = this.commonData.currentCompany.type_id;
    const industries_type = business_type === 1 ? 1 : 0;

    this.api
      .getIndustries()
      .pipe(take(1))
      .subscribe((res) => {
        // Формруем массив индустрий по категориям для селекта в зависимости от типа бизнеса
        this.industries = res['data'].reduce((acc: IndustryCategoriesList[], value: Industry): IndustryCategoriesList[] => {
          if (!value || value.type !== industries_type) {
            return acc;
          }

          const categoryIndex = acc.findIndex((i) => i.label === value.category);

          // Если категории текущей индустрии ещё нет в массиве,
          // создаем новый объект категории с массивом для индустрий данной категории
          if (categoryIndex === -1) {
            const category = { label: value.category, items: [] };
            category.items.push({ label: value.title, value: value.id });
            acc.push(category);
            // Если категория есть, добавляем индустрию в массив данной категории
          } else {
            acc[categoryIndex].items.push({ label: value.title, value: value.id });
          }

          return acc;
        }, []);
      });
  }

  getCurrencies() {
    this.api
      .getCurrencies()
      .pipe(take(1))
      .subscribe((res) => {
        this.currencies = res['data'].map((currency: Currency) => {
          return { label: currency.title, value: currency.id };
        });
      });
  }

  onSubmit() {
    const updateCompany = this.api.updateCompany(this.companyProfile.value);
    const updateUser = this.api.setUser({ language: this.companyProfile.value.language });

    forkJoin([updateCompany, updateUser])
      .pipe(take(1))
      .subscribe(([companyRes, userRes]) => {
        if (companyRes['status'] === 'success') {
          const company = { ...this.companyProfile.value };

          delete company.language;
          this.commonData.currentCompany = { ...this.commonData.currentCompany, ...company };
          this.notificationMessageService.pushAlert({ detail: 'notification-update-company' });
        }
        if (userRes['status'] === 'success') {
          this.translate.use(this.companyProfile.value.language);
          localStorage.setItem('locale', this.companyProfile.value.language);
          this.commonData.user = { ...this.commonData.user, language: this.companyProfile.value.language };
        }
      });
  }
}
