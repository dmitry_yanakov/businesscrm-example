import { Component, OnInit } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonDataService } from '@core/services/common-data.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-tariff',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.scss'],
})
export class TariffComponent implements OnInit {
  companyTypes;
  typesPrice: any;
  prices = [];
  selectedType: any;
  rateSelectionForm: FormGroup;

  constructor(
    private apiService: ApiService,
    private commonData: CommonDataService,
    private fb: FormBuilder,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.initRateSelectionForm();
    this.getCompanyTypes();
    this.getTypesPrice();
  }

  initRateSelectionForm() {
    this.rateSelectionForm = this.fb.group({
      type_id: '',
    });
  }

  getCompanyTypes() {
    this.apiService.getCompanyTypes().subscribe((response: any) => {
      if (response.status === 'success') {
        this.companyTypes = response.data;
        this.selectedType = this.companyTypes.find((type) => type.id === this.commonData.currentCompany.type_id);
        this.populateForm();
      }
    });
  }

  getTypesPrice() {
    this.apiService.getTypesPrice().subscribe((response: any) => {
      if (response.status === 'success') {
        this.typesPrice = response.data;
        this.prices = [this.typesPrice[0], this.typesPrice[3], this.typesPrice[6]];
      }
    });
  }

  populateForm() {
    this.rateSelectionForm.patchValue({
      type_id: `${this.selectedType.id}`,
    });
  }

  onSelectType(type) {
    this.selectedType = type;
    this.populateForm();
  }

  onSubmit() {
    this.apiService.updateCompanyType(this.rateSelectionForm.get('type_id').value).subscribe((res) => {
      if (res['status'] === 'success') {
        this.commonData.currentCompany.type_id = this.rateSelectionForm.value.type_id;
        this.notificationMessageService.pushAlert({ detail: 'notification-update-company-type' });
      }
    });
  }
}
