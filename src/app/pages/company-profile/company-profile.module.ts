import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { CropperModule, HeaderModule, ScrollPanelModule, SharedModule } from '../../shared';

import { CompanyProfileComponent } from './company-profile.component';
import { GeneralComponent } from './components/general/general.component';

import { CompanyProfileRoutingModule } from './company-profile-routing.module';
import { TariffComponent } from './components/tariff/tariff.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { DialogModule } from 'primeng/dialog';
import { BookingSettingsComponent } from './components/booking-settings/booking-settings.component';
import { HelpModalComponent } from './components/booking-settings/components/help-modal/help-modal.component';
import { QrCodeComponent } from './components/booking-settings/components/qr-code/qr-code.component';
import { LocationsComponent } from './components/locations/locations.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    CompanyProfileComponent,
    GeneralComponent,
    TariffComponent,
    PortfolioComponent,
    BookingSettingsComponent,
    HelpModalComponent,
    QrCodeComponent,
    LocationsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    TranslateModule,
    CompanyProfileRoutingModule,
    ScrollPanelModule,
    ReactiveFormsModule,
    CropperModule,
    DropdownModule,
    HeaderModule,
    DialogModule,
    AgmCoreModule,
  ],
})
export class CompanyProfileModule {}
