import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-business-block',
  templateUrl: './business-block.component.html',
  styleUrls: ['./business-block.component.scss']
})
export class BusinessBlockComponent implements OnInit {
  translateLangs: string[] = [];
  constructor(
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.translateLangs = this.translate.getLangs();
  }

  // easier access to translate
  get t() {
    return this.translate.instant('Business');
  }

  onChangeLanguages(language: string): void {
    this.translate.use(language);
  }

  get selectedLang() {
    return this.translate.currentLang;
  }
}
