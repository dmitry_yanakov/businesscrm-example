import { Component, OnInit } from '@angular/core';
import { CommonDataService, Title } from '@core/services/common-data.service';

@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.scss'],
})
export class DeskComponent implements OnInit {
  constructor(private commonData: CommonDataService) {}

  ngOnInit() {
    this.commonData.changeTitle(Title.dashboard);
  }
}
