import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
})
export class GeneralComponent implements OnInit {
  employeeInfoForm: FormGroup;

  @Input() display;
  @Input() isEdit;
  @Input() phoneCodes;
  @Input() roles;
  @Input() branches;
  @Input() employeeInfoFormGroup;

  constructor() {}

  ngOnInit() {
    this.initEmployeeInfoForm();
  }

  initEmployeeInfoForm() {
    this.employeeInfoForm = this.employeeInfoFormGroup;
  }

  changeIsService(event) {
    if (event.value === 5) {
      this.employeeInfoForm.patchValue({ is_service: true });
    }
  }
}
