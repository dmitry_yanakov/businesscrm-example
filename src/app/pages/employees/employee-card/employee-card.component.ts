import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { StaffService } from '@core/services/staff.service';
import { CommonDataService } from '@core/services/common-data.service';
import { HelperService } from '@core/services/helper.service';
import { TranslateService } from '@ngx-translate/core';

interface Services {
  id?: number;
  product_category_id?: number;
  product_category_title?: string;
  product_duration?: number;
  product_id?: number;
  product_price?: number;
  product_title?: string;
}

export interface Employee {
  avatar?: string;
  branch_id?: number;
  company_id?: number;
  date?: string;
  groups?: [];
  id?: number;
  is_fire?: number;
  is_service?: number;
  products?: Services[];
  role?: number;
  role_title?: string;
  user_email?: string;
  user_first_name?: string;
  user_id?: number;
  user_last_name?: string;
  user_phone_code?: number;
  user_phone_country?: string;
  user_phone_number?: number;
  user_birth_date?: string;
}

export interface CategoryProducts {
  label: string;
  value: Services;
}
export interface FilteredProductByCategory {
  label: string;
  items: CategoryProducts[];
}

@Component({
  selector: 'app-employee-card',
  // TODO: добавить перевод для "employee-have-no-related-services" и "employee-have-no-related-classes"
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss'],
})
export class EmployeeCardComponent implements OnInit, OnDestroy {
  isLoading = true;
  businessType: number;
  currency_symbol: string;
  employee: Employee = {};
  filteredProductsByCategory: FilteredProductByCategory[];
  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private location: Location,
    private activateRouter: ActivatedRoute,
    private staffService: StaffService,
    private commonDataService: CommonDataService,
    private helperService: HelperService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.businessType = this.commonDataService.currentCompany.session;
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.getEmployee();
    this.staffService.employeeUpdated.subscribe((status) => {
      if (status) {
        this.getEmployee();
      }
    });
  }

  getEmployee() {
    this.activateRouter.params
      .pipe(
        switchMap((e) => {
          return this.staffService.getEmployee(e.id);
        }),
        catchError(() => {
          return of({});
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((employee: Employee) => {
        this.employee = employee;

        const products = this.businessType === 0 ? employee.products : this.businessType === 1 ? employee.groups : [];

        this.filteredProductsByCategory = this.getFilteredProductsByCategory(products);

        this.isLoading = false;
      });
  }

  getFilteredProductsByCategory(products) {
    return products.reduce((acc, product: Services): FilteredProductByCategory[] => {
      const categoryIndex = acc.findIndex((i) => i.label === product.product_category_title);

      const product_duration =
        this.helperService.parseHours(product.product_duration, this.translate.instant('hr')) +
        ' ' +
        this.helperService.parseMinutes(product.product_duration, this.translate.instant('min'));

      if (categoryIndex === -1) {
        const category = { label: product.product_category_title, items: [] };

        category.items.push({ label: product.product_title, value: { ...product, product_duration } });
        acc.push(category);
      } else {
        acc[categoryIndex].items.push({
          label: product.product_title,
          value: { ...product, product_duration },
        });
      }

      return acc;
    }, []);
  }

  goBack() {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
