import { Component, OnInit, Input } from '@angular/core';
import { FilteredProductByCategory } from '../../employee-card.component';
import { CommonDataService } from '@core/services/common-data.service';

@Component({
  selector: 'app-employee-related-products',
  templateUrl: './employee-related-products.component.html',
  styleUrls: ['./employee-related-products.component.scss'],
})
export class EmployeeRelatedProductsComponent implements OnInit {
  businessType: number;
  currency_symbol: string;
  @Input() filteredProductsByCategory: FilteredProductByCategory[];
  constructor(private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.businessType = this.commonDataService.currentCompany.session;
  }
}
