import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { StaffService } from '@core/services/staff.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Employee } from '../../employee-card.component';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-employee-card-info',
  templateUrl: './employee-card-info.component.html',
  styleUrls: ['./employee-card-info.component.scss'],
})
export class EmployeeCardInfoComponent implements OnInit, OnDestroy {
  moment = moment;

  @Input() employee: Employee;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private location: Location,
    private staffService: StaffService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {}

  deactivateEmployee() {
    this.staffService
      .deactivateEmployee(this.employee.id)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        this.location.back();
        this.notificationMessageService.pushAlert({ detail: 'notification-remove-employee' });
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
