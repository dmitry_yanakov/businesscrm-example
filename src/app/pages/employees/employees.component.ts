import {Component, OnDestroy, OnInit} from '@angular/core';
import { StaffService } from '@core/services/staff.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class EmployeesComponent implements OnInit, OnDestroy {
  searchText = '';

  private unsubscribe: Subject<any> = new Subject();

  constructor(private staffService: StaffService) {}

  ngOnInit() {
    this.staffService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.searchText = search));
  }

  search(text) {
    if (this.searchText !== text) {
      this.searchText = text;
      this.staffService.searchEmployees(text).pipe(takeUntil(this.unsubscribe)).subscribe(() => this.staffService.loading.next(false));
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
