import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { PaymentService } from '@core/services/payment.service';
import { CommonDataService } from '@core/services/common-data.service';
import { ApiService } from '@core/services/api.service';
import { takeUntil } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss'],
})
export class PaymentHistoryComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  payments = [];
  loading = false;
  tableOptions: any = { sortField: '' };
  currency_symbol: string;
  selectedTransactionId: number;
  @ViewChild('dv', { static: false }) dv;
  private unsubscribe: Subject<any> = new Subject();

  constructor(private paymentService: PaymentService, private commonDataService: CommonDataService, private apiService: ApiService) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.paymentService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.paymentService.data.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.payments = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  loadGoodsLazy(event: LazyLoadEvent) {
    this.paymentService.loading.next(true);

    this.paymentService
      .getPayments({ offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.paymentService.loading.next(false));
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
