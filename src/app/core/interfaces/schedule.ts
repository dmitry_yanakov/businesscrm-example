export interface AvailableEmployee {
  id: number;
  user_id: number;
  employee_first_name: string;
  employee_last_name: string;
}

export interface AvailableTime {
  from_time: number;
  to_time: number;
  employees: number[];
}

export interface AvailableEmployeesAndTime {
  entities: {
    employees?: {
      [key: string]: AvailableEmployee;
    };
    times?: {
      [key: string]: AvailableTime;
    };
  };
  result?: number[];
}
