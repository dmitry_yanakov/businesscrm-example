import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from './services/api.service';
import { AppAuthService } from './services/auth.service';
import { CompanyService } from './services/company.service';
import { CommonDataService } from '@core/services/common-data.service';
import { StaffService } from '@core/services/staff.service';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [ApiService, AppAuthService, CompanyService, CommonDataService, StaffService],
  exports: [],
})
export class CoreModule {}
