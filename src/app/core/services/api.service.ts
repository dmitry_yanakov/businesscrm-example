import { throwError as observableThrowError } from 'rxjs';
import { catchError, tap, mapTo, map } from 'rxjs/operators';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonDataService } from '@core/services/common-data.service';
import { AuthService } from 'angularx-social-login';

@Injectable()
export class ApiService {
  public apiUrl = environment.apiUrl;
  private headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
  public token = '';
  public user = {};
  public avatarUpdate = new EventEmitter<boolean>();

  constructor(private http: HttpClient, private commonData: CommonDataService, private authService: AuthService) {}

  public register(registerInfo) {
    const params = new URLSearchParams();
    for (const key of Object.keys(registerInfo)) {
      params.set(key, registerInfo[key]);
    }

    return this.http.post(this.apiUrl + `/account/register`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public approveRegister(email, code) {
    const params = new URLSearchParams();
    params.set('email', email);
    params.set('email_code', code);

    return this.http.post(this.apiUrl + `/account/approve_email`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public login(email, password) {
    const params = new URLSearchParams();
    params.set('email', email);
    params.set('password', password);

    return this.http.post(this.apiUrl + `/account/login`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getSelfUser(token = null) {
    const params = new URLSearchParams();
    if (token === null) {
      params.set('token', this.token);
    } else {
      params.set('token', token);
    }

    return this.http.post(this.apiUrl + `/account/self`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getSelfCompany() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    return this.http.post(this.apiUrl + `/company/self`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getTypesPrice() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    return this.http.post(this.apiUrl + `/payment/price/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyTypes() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    return this.http.post(this.apiUrl + `/company/types`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public updateCompany(company) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));

    Object.keys(company).forEach((item) => {
      params.set(item, company[item]);
    });

    return this.http.post(this.apiUrl + `/company/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public updateCompanyType(type_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('type_id', String(type_id));

    return this.http.post(this.apiUrl + `/company/update_type`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public setUser(user) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('data', JSON.stringify(user));

    return this.http.post(this.apiUrl + `/account/user/set`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getUserAvatar(user_id) {
    return this.http.get(this.apiUrl + `/account/user/avatar?user_id=` + user_id, { responseType: 'arraybuffer' }).pipe(
      map((res) => {
        let binary = '';
        const bytes = new Uint8Array(res);
        const len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
          binary += String.fromCharCode(bytes[i]);
        }
        return 'data:image/png;base64,' + window.btoa(binary);
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyLogo(company_id) {
    return this.http.get(this.apiUrl + `/company/logo?company_id=` + company_id, { responseType: 'arraybuffer' }).pipe(
      map((res) => {
        let binary = '';
        const bytes = new Uint8Array(res);
        const len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
          binary += String.fromCharCode(bytes[i]);
        }
        return 'data:image/png;base64,' + window.btoa(binary);
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public changePassword(password, newPassword) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('old_password', password);
    params.set('new_password', newPassword);

    return this.http.post(this.apiUrl + `/account/update_password`, params.toString(), { headers: this.headers }).pipe(
      tap((res) => (res['status'] === 'success' ? localStorage.removeItem('token') : null)),
      mapTo(true),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public logOut() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/account/logout`, params.toString(), { headers: this.headers }).pipe(
      tap((res) => {
        localStorage.removeItem('token');
        this.token = null;
        this.commonData.user = {};
        this.commonData.companies = [];
        this.commonData.branches = [];
        this.commonData.currentCompany = {};
        this.commonData.currentBranch = {};
        this.commonData.userLocation = {};
        this.commonData.isCompanyReadySubject$.next(false);
        this.authService.signOut().catch((error) => {
          return observableThrowError(error);
        });
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public forgot(email) {
    const params = new URLSearchParams();
    params.set('email', email);

    return this.http.post(this.apiUrl + `/account/forgot`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCurrencies() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/currency/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public forgotFinish(code, password) {
    const params = new URLSearchParams();
    params.set('forgot_code', code);
    params.set('password', password);

    return this.http.post(this.apiUrl + `/account/forgot_change`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createCompany(company) {
    const params = new URLSearchParams();
    params.set('token', this.token);

    Object.keys(company).forEach((item) => {
      if (company[item]) {
        params.set(item, company[item]);
      }
    });

    return this.http.post(this.apiUrl + `/company/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyById(id) {
    const params = new URLSearchParams();
    params.set('company_id', id);

    return this.http.post(this.apiUrl + `/company/id`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getEmployee(employee_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('employee_id', employee_id);

    return this.http.post(this.apiUrl + `/employee/id`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getEmployees({
    company_id = this.commonData.currentCompany.id,
    branch_id = this.commonData.currentBranch.id,
    is_fire = 0,
    offset = 0,
    count = 7,
    order = 'employees.date DESC',
    search = '',
  }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(company_id));
    params.set('branch_id', String(branch_id));
    params.set('is_fire', String(is_fire)); // 0
    params.set('offset', String(offset)); // 0
    params.set('count', String(count)); // 20
    params.set('order', order); // employees.id ASC
    params.set('search', search);

    return this.http.post(this.apiUrl + `/employee/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createEmployee(employee) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));

    Object.keys(employee).forEach((item) => {
      params.set(item, employee[item]);
    });

    return this.http.post(this.apiUrl + `/employee/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public editEmployee(employee) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));

    Object.keys(employee).forEach((item) => {
      params.set(item, employee[item]);
    });

    return this.http.post(this.apiUrl + `/employee/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public addEmployeeProducts(employee_id, products: string) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('employee_id', employee_id);
    params.set('products', products);

    return this.http.post(this.apiUrl + `/employee/add_product`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public restoreEmployee(employee_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('employee_id', employee_id);

    return this.http.post(this.apiUrl + `/employee/restore`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public deactivateEmployee(employee_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('employee_id', employee_id);

    return this.http.post(this.apiUrl + `/employee/fire`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getRoles() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/employee/roles`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCategory({ branch_id = this.commonData.currentBranch.id, product_type }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(branch_id));
    params.set('product_type', String(product_type));

    return this.http.post(this.apiUrl + `/product/category/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createCategory({ branch_id = this.commonData.currentBranch.id, product_type, title, description = '...' }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(branch_id));
    params.set('title', title);
    params.set('description', description);
    params.set('product_type', String(product_type));

    return this.http.post(this.apiUrl + `/product/category/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public editCategory({ category_id, title, description = '...' }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('category_id', String(category_id));
    params.set('description', description);
    params.set('title', title);

    return this.http.post(this.apiUrl + `/product/category/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createProduct(product) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('title', String(product.title).trim());
    params.set('type', product.type);

    if (product.vendor) {
      params.set('vendor', String(product.vendor).trim());
    }
    if (product.description) {
      params.set('description', String(product.description).trim());
    }
    if (product.cost) {
      params.set('cost', product.cost);
    }
    if (product.price) {
      params.set('price', product.price);
    }
    if (product.price_max) {
      params.set('price_max', product.price_max);
    }
    if (product.limit) {
      params.set('limit', product.limit);
    }
    if (product.activate) {
      params.set('activate', product.activate);
    }
    if (product.time) {
      params.set('time', product.time);
    }
    if (product.category_id) {
      params.set('category_id', product.category_id);
    }
    if (product.duration) {
      const duration = Number(product.duration.slice(0, 2)) * 60 + Number(product.duration.slice(3));

      params.set('duration', String(duration));
    }
    if (Array.isArray(product.employees)) {
      params.set('employees', JSON.stringify(product.employees));
    }
    if (Array.isArray(product.relations)) {
      params.set('relations', JSON.stringify(product.relations));
    }

    return this.http.post(this.apiUrl + `/product/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public editProduct(product) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('product_id', product.product_id);
    params.set('title', String(product.title).trim());
    params.set('type', product.type);

    if (product.vendor) {
      params.set('vendor', String(product.vendor).trim());
    }
    if (product.description) {
      params.set('description', String(product.description).trim());
    }
    if (product.cost) {
      params.set('cost', product.cost);
    }
    if (product.price) {
      params.set('price', product.price);
    }
    if (product.price_max) {
      params.set('price_max', product.price_max);
    }
    if (product.limit) {
      params.set('limit', product.limit);
    }
    if (product.activate) {
      params.set('activate', product.activate);
    }
    if (product.time) {
      params.set('time', product.time);
    }
    if (product.category_id) {
      params.set('category_id', product.category_id);
    }
    if (product.duration) {
      const duration = Number(product.duration.slice(0, 2)) * 60 + Number(product.duration.slice(3));
      console.log(duration);
      params.set('duration', String(duration));
    }
    if (Array.isArray(product.employees)) {
      params.set('employees', JSON.stringify(product.employees));
    }
    if (Array.isArray(product.relations)) {
      params.set('relations', JSON.stringify(product.relations));
    }

    return this.http.post(this.apiUrl + `/product/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getProducts({
    branch_id = this.commonData.currentBranch.id,
    product_type,
    is_archive = 0,
    offset = 0,
    count = 7,
    order = 'products.date DESC',
    search = '',
    product_id: id,
  }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(branch_id));
    params.set('type', String(product_type));
    params.set('is_archive', String(is_archive));
    params.set('offset', String(offset));
    params.set('count', String(count));
    params.set('order', order);
    params.set('search', search);

    if (id) {
      params.set('id', String(id));
    }

    return this.http.post(this.apiUrl + `/product/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public toArchive(product_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('product_id', product_id);

    return this.http.post(this.apiUrl + `/product/to_archive`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public fromArchive(product_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('product_id', product_id);

    return this.http.post(this.apiUrl + `/product/from_archive`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getIndustries() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/company/industries`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyPhotos() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));

    return this.http.post(this.apiUrl + `/company/photo_get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public addCompanyPhoto(photo, is_main = 0) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('file', photo);
    params.set('is_main', String(is_main));

    return this.http.post(this.apiUrl + `/company/photo_add`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public removeCompanyPhoto(photo_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('photo_id', photo_id);

    return this.http.post(this.apiUrl + `/company/photo_remove`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyBranch(company_id = this.commonData.currentCompany.id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(company_id));

    return this.http.post(this.apiUrl + `/branch/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getAddressByLatLng(lat, lng) {
    return this.http
      .get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ' ' + lng + '&key=AIzaSyCokv39DsotQK9pui7dzIpbuQXjGHItq-k')
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public changeEmail(email, password) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('new_email', email);
    params.set('password', password);

    return this.http.post(this.apiUrl + `/account/update_email`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  /* Start Notification */
  public getNotifications(notification_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('branch_id', String(this.commonData.currentBranch.id));
    if (notification_id >= 0) {
      params.set('notification_id', String(notification_id));
    }

    return this.http.post(this.apiUrl + `/notification/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clearNotification(notification_id = 0) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('notification_id', String(notification_id));

    return this.http.post(this.apiUrl + `/notification/clear`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public showNotification(notification_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('notification_id', notification_id);

    return this.http.post(this.apiUrl + `/notification/show`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Notification */

  public getPhoneCodes() {
    return this.http.get(this.apiUrl + `/location/get`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getUserLocation() {
    return this.http.get(this.apiUrl + `/location/ip`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public loginWithGoogle(googleId, googleFirstName, googleLastName, googleEmail) {
    const params = new URLSearchParams();
    params.set('google_id', googleId);
    params.set('google_first_name', googleFirstName);
    params.set('google_last_name', googleLastName);
    params.set('google_email', googleEmail);
    params.set('language', 'en');

    return this.http.post(this.apiUrl + `/account/google`, params.toString(), { headers: this.headers }).pipe(
      // tslint:disable-next-line:no-string-literal
      tap((res) => {
        localStorage.setItem('token', res['data']['token']);
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public loginWithFB(facebookId, facebookFirstName, facebookLastName, facebookEmail, facebookBirthday, facebookGender) {
    const params = new URLSearchParams();
    params.set('facebook_id', facebookId);
    params.set('facebook_first_name', facebookFirstName);
    params.set('facebook_last_name', facebookLastName);
    params.set('facebook_email', facebookEmail);
    params.set('facebook_birthday', facebookBirthday);
    params.set('facebook_gender', facebookGender);
    params.set('language', 'en');

    return this.http.post(this.apiUrl + `/account/facebook`, params.toString(), { headers: this.headers }).pipe(
      // tslint:disable-next-line:no-string-literal
      tap((res) => {
        localStorage.setItem('token', res['data']['token']);
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  /* Create Location */
  public createBranch({ phones: [], title, description = '...', country, state, city, street, zip, latitude, longitude }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('title', title);
    params.set('description', description);
    params.set('country', country);
    params.set('state', state);
    params.set('city', city);
    params.set('street', street);
    params.set('zip', zip);
    params.set('latitude', latitude);
    params.set('longitude', longitude);

    return this.http.post(this.apiUrl + `/branch/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public branchTime({ branch_id, day, time, is_active }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);
    params.set('day', day);
    params.set('time', time);
    params.set('is_active', is_active);

    return this.http.post(this.apiUrl + `/branch/time`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public updateBranch({ phones: [], title, description = '...', country, branch_id, state, city, street, zip, latitude, longitude }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('title', title);
    params.set('description', description);
    params.set('country', country);
    params.set('branch_id', branch_id);
    params.set('state', state);
    params.set('city', city);
    params.set('street', street);
    params.set('zip', zip);
    params.set('latitude', latitude);
    params.set('longitude', longitude);

    return this.http.post(this.apiUrl + `/branch/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public deactivateBranch({ branch_id }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);

    return this.http.post(this.apiUrl + `/branch/deactivate`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public activateBranch({ branch_id }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);

    return this.http.post(this.apiUrl + `/branch/activate`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public breakAdd({ branch_id, day, from, to }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);
    params.set('day', day);
    params.set('from', from);
    params.set('to', to);

    return this.http.post(this.apiUrl + `/branch/break_add`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public breakRemove({ break_id }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('break_id', break_id);

    return this.http.post(this.apiUrl + `/branch/break_remove`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Create Location */

  /* Finance */
  public createIncome(branch_id, employee_id, client_id, products, discount = 0) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);
    params.set('employee_id', employee_id);
    params.set('client_id', client_id);
    params.set('products', products);
    params.set('discount', String(discount));

    return this.http.post(this.apiUrl + `/transaction/create_income`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createOutcome(branch_id, products) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);
    params.set('products', products);

    return this.http.post(this.apiUrl + `/transaction/create_outcome`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public removeTransaction(transaction_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('transaction_id', transaction_id);

    return this.http.post(this.apiUrl + `/transaction/remove`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getTransaction({
    type = 0,
    branch_id = this.commonData.currentBranch.id,
    offset = 0,
    count = 7,
    order = 'transactions.date DESC',
    search = '',
  }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('type', String(type));
    params.set('branch_id', String(branch_id));
    params.set('offset', String(offset)); // 0
    params.set('count', String(count)); // 20
    params.set('order', order);
    params.set('search', search);

    return this.http.post(this.apiUrl + `/transaction/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Finance */

  public createClient({ source_id, first_name, last_name, email, phone_country, phone_code, phone_number, birth, sex, comment, avatar }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('first_name', first_name);
    params.set('phone_country', phone_country);
    params.set('phone_code', phone_code);
    params.set('phone_number', phone_number);
    if (last_name) {
      params.set('last_name', last_name);
    }
    if (email) {
      params.set('email', email);
    }
    if (birth) {
      params.set('birth', birth);
    }
    if (sex) {
      params.set('sex', sex);
    }
    if (source_id) {
      params.set('source_id', source_id);
    }
    if (comment) {
      params.set('comment', comment);
    }
    if (avatar) {
      params.set('avatar', avatar);
    }

    return this.http.post(this.apiUrl + `/client/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getClients({ branch_id = this.commonData.currentBranch.id, offset = 0, count = 7, order = 'clients.date DESC', search = '' }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(branch_id));
    params.set('offset', String(offset)); // 0
    params.set('count', String(count)); // 20
    params.set('order', order);
    params.set('search', search);

    return this.http.post(this.apiUrl + `/client/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getClient(client_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('client_id', client_id);

    return this.http.post(this.apiUrl + `/client/id`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public updateClient({
    client_id,
    source_id,
    first_name,
    last_name,
    email,
    phone_country,
    phone_code,
    phone_number,
    birth,
    sex,
    comment,
    avatar,
    is_ban,
  }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('client_id', client_id);
    params.set('first_name', first_name);
    params.set('phone_country', phone_country);
    params.set('phone_code', phone_code);
    params.set('phone_number', phone_number);
    params.set('is_ban', is_ban);
    if (last_name) {
      params.set('last_name', last_name);
    }
    if (email) {
      params.set('email', email);
    }
    if (birth) {
      params.set('birth', birth);
    }
    if (sex) {
      params.set('sex', sex);
    }
    if (source_id) {
      params.set('source_id', source_id);
    }
    if (comment) {
      params.set('comment', comment);
    }
    if (avatar) {
      params.set('avatar', avatar);
    }

    return this.http.post(this.apiUrl + `/client/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public banClient(client_id, is_ban) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('client_id', client_id);
    params.set('is_ban', is_ban);

    return this.http.post(this.apiUrl + `/client/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public inviteClient(client_id, email, phone) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('client_id', client_id);
    params.set('email', email);
    params.set('phone', phone);

    return this.http.post(this.apiUrl + `/client/invite`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  /* Client import */
  public clientImport({ branch_id }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', branch_id);

    return this.http.post(this.apiUrl + `/client/import`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clientExport({ branch_id }) {
    return this.http
      .get(
        this.apiUrl + `/client/import?token=` + this.token + `&company_id` + this.commonData.currentCompany.id + `&branch_id=` + branch_id
      )
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }
  /* End Client import */

  /* Client note */
  public clientNoteCreate({ client_id, note }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('client_id', client_id);
    params.set('note', note);

    return this.http.post(this.apiUrl + `/client/note/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clientNoteUpdate(note_id, note) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('note_id', note_id);
    params.set('note', note);

    return this.http.post(this.apiUrl + `/client/note/update`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clientNoteRemove(note_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('note_id', note_id);

    return this.http.post(this.apiUrl + `/client/note/remove`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clientNoteGet(client_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('client_id', String(client_id));

    return this.http.post(this.apiUrl + `/client/note/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Client note */

  public clientSourceGet() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));

    return this.http.post(this.apiUrl + `/client/source/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  /* Payment */
  public getPayments({ offset = 0, count = 7, order = 'payments.date DESC' }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('offset', String(offset)); // 0
    params.set('count', String(count)); // 20
    params.set('order', order);
    return this.http.post(this.apiUrl + `/payment/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public payInterKassa(payment_price_id, currency = 'USD') {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('payment_price_id', payment_price_id);
    params.set('currency', currency);
    return this.http.post(this.apiUrl + `/payment/pay/interkassa`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public payLiqPay(payment_price_id, currency = 'USD') {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(this.commonData.currentCompany.id));
    params.set('payment_price_id', payment_price_id);
    params.set('currency', currency);
    return this.http.post(this.apiUrl + `/payment/pay/liqpay`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Payment*/

  /* Start Booking */
  public createBooking({ product_id, employee_id, from_date, comment = '', repeat_from, repeat_to, is_formed, color }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('product_id', product_id);

    if (employee_id) {
      params.set('employee_id', employee_id);
    }

    params.set('from_date', from_date);
    params.set('comment', comment);

    if (repeat_from) {
      params.set('repeat_from', repeat_from);
    }

    if (repeat_to) {
      params.set('repeat_to', repeat_to);
    }

    if (is_formed) {
      params.set('is_formed', is_formed);
    }

    params.set('color', color);

    return this.http.post(this.apiUrl + `/booking/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  addBookingClient({ booking_id, clients }: { booking_id: number; clients: string }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('clients', clients);

    return this.http.post(this.apiUrl + `/booking/add_client`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  acceptClient({ booking_id, client_id }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('client_id', String(client_id));

    return this.http.post(this.apiUrl + `/booking/accept_client`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  // Set client status to 4
  rejectClient({ booking_id, client_id }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('client_id', client_id);

    return this.http.post(this.apiUrl + `/booking/reject_client`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  // Set client status to 5
  cancelClient({ booking_id, client_id }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('client_id', client_id);

    return this.http.post(this.apiUrl + `/booking/cancel_client`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  // Remove client from event
  removeClient({ booking_id, client_id }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('client_id', client_id);

    return this.http.post(this.apiUrl + `/booking/remove_client`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  // Remove client from event
  removeBooking(booking_id) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));

    return this.http.post(this.apiUrl + `/booking/remove`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  changeBookingStatus({ booking_id, status }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('booking_id', String(booking_id));
    params.set('status', String(status));

    return this.http.post(this.apiUrl + `/booking/change_status`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getEmployeesAvailableTime({ product_id, employee_id, current_date, date }) {
    const params = new URLSearchParams();

    params.set('token', this.token);
    params.set('branch_id', String(this.commonData.currentBranch.id));
    params.set('product_id', product_id);
    if (employee_id) {
      params.set('employee_id', employee_id);
    }
    params.set('current_date', current_date);
    params.set('date', date);

    return this.http.post(this.apiUrl + `/booking/time`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  /* End Booking */
}
