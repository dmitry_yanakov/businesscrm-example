import { Component, OnInit } from '@angular/core';

export interface RouteInfo {
  path: string;
  title: string;
  icontype: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
  {
    path: '/desk',
    title: 'dashboard',
    icontype: 'icon-home',
  },
  {
    path: '/schedule',
    title: 'calendar',
    icontype: 'icon-calendar',
  },
  {
    path: '/clients/list',
    title: 'clients',
    icontype: 'icon-clients',
  },
  {
    path: '/finance/income',
    title: 'wallet',
    icontype: 'icon-wallet',
  },
  {
    path: '/reports',
    title: 'reports',
    icontype: 'icon-reports',
  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  display = false;
  selectedIndex: number;
  menuItems = [];
  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => menuItem);
  }

  select(index: number) {
    this.selectedIndex = index;
  }
}
