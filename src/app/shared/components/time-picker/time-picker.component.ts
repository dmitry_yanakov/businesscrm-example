import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  Renderer2,
  forwardRef,
  HostListener,
  AfterViewInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimePickerComponent),
      multi: true,
    },
  ],
})
export class TimePickerComponent implements OnInit, ControlValueAccessor {
  isOpen = false;
  hours: string[] = [];
  minutes: string[] = [];
  selectedHour = null;
  selectedMinute = null;
  _selectedTime;

  onChange: (value: string) => void;
  onTouched: () => void;

  @Input() inputId;
  @Input() hoursStep = 1;
  @Input() minutesStep = 1;
  @Input() hoursPrefix = '';
  @Input() minutesPrefix = '';
  @Input() placeholder = '';

  @ViewChild('timePicker', { static: true }) timePicker: ElementRef;
  @ViewChild('timePickerInput', { static: false }) input: ElementRef;
  @ViewChild('timePickerList', { static: false }) list: ElementRef;

  constructor(private renderer: Renderer2, private translate: TranslateService) {}

  ngOnInit() {
    this.hours = this.generateOptions(24, this.hoursStep);
    this.minutes = this.generateOptions(60, this.minutesStep);
  }

  @HostListener('window:click', ['$event'])
  hideModal(event) {
    if (this.isOpen && !this.timePicker.nativeElement.contains(event.target)) {
      this.isOpen = false;
    }
  }

  get selectedTime() {
    if (!this._selectedTime) {
      return '';
    }

    const [hour, minute] = this._selectedTime.split(':');

    return this._selectedTime
      ? `${hour}${this.translate.instant(this.hoursPrefix)} ${minute}${this.translate.instant(this.minutesPrefix)}`
      : '';
  }

  setTime(value, type) {
    if (!this._selectedTime) {
      this._selectedTime = '00:00';
    }

    if (type === 'hour') {
      const [hour, minute] = this._selectedTime.split(':');

      this.selectedHour = value;
      this._selectedTime = `${value}:${minute}`;
      this.onChange(this._selectedTime);
    } else if (type === 'minute') {
      const [hour, minute] = this._selectedTime.split(':');

      this.selectedMinute = value;
      this._selectedTime = `${hour}:${value}`;
      this.onChange(this._selectedTime);
    }
  }

  generateOptions(length, step = 1) {
    const arr = [];
    for (let value = 0; value < length; value += step) {
      arr.push(this.formatOption(value));
    }
    return arr;
  }

  formatOption(option) {
    let value = `${option}`;
    if (option < 10) {
      value = `0${option}`;
    }

    return value;
  }

  public toggle() {
    this.isOpen = !this.isOpen;
  }

  // вызовет форму если значение изменилось извне
  writeValue(time): void {
    if (!time) {
      return;
    }
    this._selectedTime = time;
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
