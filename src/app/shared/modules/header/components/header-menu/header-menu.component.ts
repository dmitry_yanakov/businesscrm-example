import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

// Service
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss'],
})
export class HeaderMenuComponent implements OnInit {
  isReady = false;
  isMenuOpen = false;
  isCompaniesOpen = false;
  user_name = '';
  activeCompany = null;
  @ViewChild('userMenu', { static: false })
  userMenu: ElementRef;
  companies = [];

  constructor(private router: Router, private api: ApiService, private commonData: CommonDataService) {}

  ngOnInit() {
    if (!this.isReady) {
      const timer = setInterval(() => {
        this.isReady = !!this.commonData.currentCompany['id'];

        if (this.isReady) {
          this.user_name = this.commonData.user.first_name;
          this.initMenuCompanies();
          clearInterval(timer);
        }
      }, 25);
    } else {
      this.initMenuCompanies();
    }
  }

  initMenuCompanies() {
    this.companies = this.commonData.companies.map((company) => {
      const _company = { ...company, is_current: company.id === this.commonData.currentCompany.id };

      return { label: company.title, value: _company, branches: [] };
    });

    this.companies.forEach((company) => {
      this.api
        .getCompanyLogo(company.id)
        .pipe(take(1))
        .subscribe((response) => {
          company.logo = response;
        });
    });
  }

  getCompanyBranches(event, company) {
    if (!company.branches.length) {
      this.api.getCompanyBranch(company.value.id).subscribe((res) => {
        if (res['status'] === 'success') {
          company.branches = res['data']['branches'].map((branch) => {
            const _branch = { ...branch, is_current: this.commonData.currentBranch.id === branch.id };

            return { label: branch.title, value: _branch };
          });
        }
      });
    }
  }

  setCurrentBranch(event, company, branch) {
    this.api.setUser({ company_id: company['id'], branch_id: branch['id'] }).subscribe((res) => {
      if (res['status'] === 'success') {
        this.commonData.currentCompany = company;
        this.commonData.currentBranch = branch;
        this.commonData.companyUpdate.emit(true);
        this.updateComponent();
      }

      this.activeCompany = null;
      this.isMenuOpen = false;
      this.isCompaniesOpen = false;
    });
  }

  updateComponent() {
    this.companies.forEach((company) => {
      company.value.is_current = company.value.id === this.commonData.currentCompany.id;
      company.branches.forEach((branch) => {
        branch.value.is_current = branch.value.id === this.commonData.currentBranch.id;
      });
    });
  }

  @HostListener('document:click', ['$event'])
  closeMenu(event) {
    if (this.isMenuOpen && !this.userMenu.nativeElement.contains(event.target)) {
      this.isMenuOpen = false;
    }
  }

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  onCompanyMouseEnter(event, company) {
    this.activeCompany = company;
  }

  onCompanyMouseLeave() {
    this.activeCompany = null;
  }

  toggleCompanies() {
    this.isCompaniesOpen = !this.isCompaniesOpen;
  }

  logout() {
    this.api.logOut().subscribe(() => this.router.navigate(['/login']));
  }
}
