import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsActivityComponent } from './notifications-activity.component';

describe('NotificationsActivityComponent', () => {
  let component: NotificationsActivityComponent;
  let fixture: ComponentFixture<NotificationsActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationsActivityComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
