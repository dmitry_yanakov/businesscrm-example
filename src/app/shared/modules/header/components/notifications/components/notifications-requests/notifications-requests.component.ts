import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NotificationsService } from '@core/services/notifications.service';
import { BehaviorSubject } from 'rxjs';
import { ScheduleService } from '@core/services/schedule.service';

@Component({
  selector: 'app-notifications-requests',
  templateUrl: './notifications-requests.component.html',
  styleUrls: ['./notifications-requests.component.scss'],
})
export class NotificationsRequestsComponent implements OnInit, AfterViewInit {
  moment = moment;
  all_requests = [];
  requests = [];
  requests_limit: BehaviorSubject<number> = new BehaviorSubject<number>(6);

  constructor(private notificationsService: NotificationsService, private scheduleService: ScheduleService) {}

  ngOnInit() {
    this.notificationsService.notifications.pipe().subscribe((notifications) => {
      this.all_requests = [];
      notifications.forEach((notification) => {
        if (notification.type === 'booking_request') {
          this.all_requests.push(notification);

          if (!notification.is_show) {
            this.checkRequest(notification);
          }
        }
      });

      this.all_requests.sort((a, b) => b.id - a.id);
      this.requests = [...this.all_requests.slice(0, this.requests_limit.getValue())];
    });

    this.requests_limit.subscribe((limit) => {
      this.requests = [...this.all_requests.slice(0, limit)];
    });
  }

  ngAfterViewInit() {}

  getMoreNotifications() {
    const requests_count = this.all_requests.length;
    const requests_limit = this.requests_limit.getValue();

    if (requests_count === requests_limit) {
      return;
    } else if (requests_limit <= requests_count) {
      this.requests_limit.next(requests_limit + 6);
    }
  }

  checkRequest(request) {
    this.notificationsService.checkNotification(request).subscribe((res) => {
      if (res['status'] === 'success') {
        request.is_show = 1;
      }
    });
  }

  confirmRequest(request) {
    this.scheduleService
      .acceptClient({ booking_id: request.text.booking_id, client_id: request.text.client_id, notification_id: request.id })
      .subscribe((res) => {});
  }

  rejectRequest(request) {
    this.scheduleService
      .rejectClient({ booking_id: request.text.booking_id, client_id: request.text.client_id, notification_id: request.id })
      .subscribe(() => {});
  }
}
