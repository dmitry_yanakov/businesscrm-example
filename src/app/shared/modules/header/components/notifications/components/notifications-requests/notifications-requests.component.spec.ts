import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsRequestsComponent } from './notifications-requests.component';

describe('NotificationsRequestsComponent', () => {
  let component: NotificationsRequestsComponent;
  let fixture: ComponentFixture<NotificationsRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationsRequestsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
