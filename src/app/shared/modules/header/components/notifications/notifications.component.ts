import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '@core/services/notifications.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  notifications = [];
  requests = [];
  new_notifications_count = 0;
  new_requests_count = 0;
  selected_tab = 0;
  constructor(private notificationsService: NotificationsService) {}

  ngOnInit() {
    this.notificationsService.notifications.pipe().subscribe((notifications) => {
      this.notifications.push(
        ...notifications.filter((notification) => notification.type !== 'booking_request').sort((a, b) => a.id - b.id)
      );
      this.requests.push(...notifications.filter((notification) => notification.type === 'booking_request').sort((a, b) => a.id - b.id));
    });

    this.notificationsService.new_notifications_count.pipe().subscribe((count) => {
      this.new_notifications_count = count;
    });

    this.notificationsService.new_requests_count.pipe().subscribe((count) => {
      this.new_requests_count = count;
    });
  }

  clearNotifications() {
    this.notificationsService.clearNotification().subscribe(() => {});
  }

  changeTab(tab_index) {
    this.selected_tab = tab_index;
  }
}
